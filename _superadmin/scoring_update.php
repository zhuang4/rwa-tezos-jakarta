<? include '../include/superadmin_authen.php';?>
<?

$id = $_REQUEST['id'];
$score = $_REQUEST['score'];
$debt = $_REQUEST['debt'];
$apr = $_REQUEST['apr'];

include '../include/database.php';
$db = new Database();  
$db->connect();


$param = array(
		"score" => $score,
		"max_borrow" => $debt,
		"apr" => $apr,
			);

if ($db->update("loan_documents_v2", $param, "id=$id"))
{
		$username = $_SESSION['username'];
		
		$param = array(
			"docid" => $id,
			"debt" => $debt,
			"score" => $score,
			"approve_by" => $username,
			);
	$db->insert("loan_approved_log", $param);

	header("location:scoring_list.php");
}
else 
{
	?>
<script>
	alert("Update fail, please try again");
	window.history.back();
</script>
	<?
}
?>