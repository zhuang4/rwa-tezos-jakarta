<?

include '../include/database.php';
$db = new Database();  
$db->connect();

$id = $_REQUEST['id'];
$score = $_REQUEST['score'];
$apr = $_REQUEST['apr'];
$debt = $_REQUEST['debt'];

$param = array(
	"score" => $score,
	"apr" => $apr,
	"max_borrow" => $debt,
	"status" => 1,
	"scoredate" => date("Y-m-d H:i:s"),
			);

if ($db->update("loan_widget_docs", $param, "id=$id")) {
	?>
	<script>
		alert('Success');
		window.location.href = 'loan_widget.php';
	</script>
	<?
} else {
	?>
	<script>
		alert('Fail');
		window.history.back();
	</script>
	<?
}

?>